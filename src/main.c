#include "test.h"

int main() {

    int tests_passed = 0;

    tests_passed += simple_mem_alloc_test();
    tests_passed += free_one_of_several_test();
    tests_passed += free_two_of_several_test();
    tests_passed += grow_heap_test();
    tests_passed += heap_not_continuous_test();

    if (tests_passed == NUM_OF_TESTS) {
	printf("all tests has been successfully passed\n");
    } else {
	printf("some of tests failed\n");
    }

    return 0;
}

