#define _DEFAULT_SOURCE

#include "test.h"

int free_heap(void *heap, size_t length) {
    return munmap(heap, size_from_capacity((block_capacity) {.bytes = length}).bytes);
}

int simple_mem_alloc_test() {

    printf("Test 1 started...\n");

    void *heap = heap_init(capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes);

    debug_heap(stdout, heap);

    void *data = _malloc(400);

    debug_heap(stdout, heap);

    if (!data) {
        printf("Test 1 failed!\n");
        return 0;
    }

    _free(data);

    debug_heap(stdout, heap);

    free_heap(heap, REGION_MIN_SIZE);


    printf("Test 1 has been passed successfully.\n");
    return 1;
}

int free_one_of_several_test() {

    printf("Test 2 started...\n");

    void *heap = heap_init(capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes);

    debug_heap(stdout, heap);

    void *data1 = _malloc(200);
    void *data2 = _malloc(200);

    debug_heap(stdout, heap);

    _free(data1);

    debug_heap(stdout, heap);

    if (!data1 || !data2) {
        printf("Test 2 failed!\n");
        return 0;
    }

    _free(data2);

    free_heap(heap, REGION_MIN_SIZE);

    printf("Test 2 has been passed successfully\n");

    return 1;
}

int free_two_of_several_test() {

    printf("Test 3 started...\n");

    void *heap = heap_init(capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes);

    debug_heap(stdout, heap);

    void *data1 = _malloc(150);
    void *data2 = _malloc(150);
    void *data3 = _malloc(200);

    debug_heap(stdout, heap);

    _free(data3);
    _free(data1);

    debug_heap(stdout, heap);

    if (!data1 || !data2 || !data3) {
        printf("Test 3 failed!\n");
        return 0;
    }

    _free(data3);

    free_heap(heap, REGION_MIN_SIZE);

    printf("Test 3 has been passed successfully\n");

    return 1;
}

int grow_heap_test() {

    printf("Test 4 started...\n");

    void *heap = heap_init(capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes);

    debug_heap(stdout, heap);

    void *data = _malloc(REGION_MIN_SIZE * 3 / 2);

    debug_heap(stdout, heap);

    struct block_header *header = heap;

    if (data != HEAP_START + offsetof(struct block_header, contents) ||
		header->capacity.bytes <= REGION_MIN_SIZE) {
	printf("Test 4 failed!\n");
	return 0;
    }

    _free(data);
    free_heap(heap, REGION_MIN_SIZE * 2);

    printf("Test 4 has been passed successfully\n");

    return 1;
}


int heap_not_continuous_test() {


    printf("Test 5 started...\n");

    void *heap = heap_init(capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes);

    debug_heap(stdout, heap);

    (void) mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void *data = _malloc(10000);

    if (!data) {

        printf("Test 5 failed!");

    }

    debug_heap(stdout, heap);

    printf("Test 5 has been passed successfully\n");

    return 1;

}

