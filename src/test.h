
#ifndef MEMORY_ALLOCATOR_TEST_H
#define MEMORY_ALLOCATOR_TEST_H
#define NUM_OF_TESTS 5

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

int simple_mem_alloc_test();

int free_one_of_several_test();

int free_two_of_several_test();

int grow_heap_test();

int heap_not_continuous_test();

#endif
